---
title: 'Task Clase 5: Taller de R Estadística y Programación'
output: 
 html_document:
   self_contained: FALSE
---

```{r setup, include=FALSE}
rm(list=ls())
library(pacman)
p_load(tutorial,tidyverse,testthat,testwhat)
tutorial::go_interactive(height = 800)
```

<!--html_preserve-->
```{r ex="create_a", type="pre-exercise-code"}
library(dplyr)
```

```{r ex="create_a", type="sample-code"}
# Punto 1
# Crear 4 vectores, que tengan lo siguiente:
# Un vector llamado ID, que tendra un rango del 1:10
# Un vector llamado Nombres, en el pondras 10 nombres de tu seleccion 
# Un vector llamado Edades, en este colocaras 10 numeros aleatorios 
# Un vector llamado posicion, en el pondras estudiante 9 veces y un NA
  
# Punto 2
# Crear un data.frame con el id, nombres y edad
# y otro data.frame con el id, nombres y posicion.

# Punto 3
# Poner en minuscula con la funcion select_all los dos data.frame creados anteriormente.

# Punto 4
# Unir los dos data.frame creados anteriormente.

# Punto 5
# Quitar las filas que contengan valores NA.
 
# Punto 6
# Utilizar la funcion subset para dejar a los estudiantes que sean mayores o igual a  14 años.

# Punto 7
# Exportar el resultado final del data.frame en formato rds.  
 
```

```{r ex="create_a", type="solution"}
# Punto 1
# Crear 4 vectores, que tengan lo siguiente:
# Un vector llamado ID, que tendra un rango del 1:10
# Un vector llamado Nombres, en el pondras 10 nombres de tu seleccion 
# Un vector llamado Edades, en este colocaras 10 numeros aleatorios 
# Un vector llamado posicion, en el pondras estudiante 9 veces y un NA

ID <- 1:10

Nombres <- c("andre", "suzy", "jhan", "jose",
             "mau","jacob", "maria", "katy", "luisa", "karen")
                     

Edades <- round(rnorm(10, mean = 14, sd = 1))

Posicion <- c( NA,rep("estudiante", 9))

# Punto 2
# Crear un data.frame con el id, nombres y edad
# y otro data.frame con el id, nombres y posicion.

base_1 <- data.frame(ID = ID, Nombre = Nombres, Edad= Edades)

base_2 <- data.frame(ID = ID, Nombre = Nombres, Posicion= Posicion)

# Punto 3
# Poner en minuscula con la funcion select_all los dos data.frame creados anteriormente.

base_1= base_1 %>% select_all(tolower)

base_2= base_2 %>% select_all(tolower)

# Punto 4
# Unir los dos data.frame creados anteriormente.

base= full_join(x= base_1, y=base_2, by= c("id", "nombre"))

# Punto 5
# Quitar las filas que contengan valores NA.

base= base %>% drop_na()

# Punto 6
# Utilizar la funcion subset para dejar a los estudiantes que sean mayores o igual a  14 años.

 base_final= base %>% subset(edad >= 14 )
 

```
```{r ex="create_a", type="sct"}
ex() %>% check_object("base_final") %>% check_equal()
success_msg("Well done!")
```
<!--/html_preserve-->
